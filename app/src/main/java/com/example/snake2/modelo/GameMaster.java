package com.example.snake2.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.jar.Manifest;

public class GameMaster {
    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    public static int getAncho() {
        return ancho;
    }

    public static int getAlto() {
        return alto;
    }

    public int getArriba() {
        return arriba;
    }

    public void setArriba(int arriba) {
        this.arriba = arriba;
    }

    public int getAbajo() {
        return abajo;
    }

    public void setAbajo(int abajo) {
        this.abajo = abajo;
    }

    public int getDerecha() {
        return derecha;
    }

    public void setDerecha(int derecha) {
        this.derecha = derecha;
    }

    public int getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(int izquierda) {
        this.izquierda = izquierda;
    }

    public int getNada() {
        return nada;
    }

    public void setNada(int nada) {
        this.nada = nada;
    }

    public int getMuro() {
        return muro;
    }

    public void setMuro(int muro) {
        this.muro = muro;
    }

    public int getCabeza() {
        return cabeza;
    }

    public void setCabeza(int cabeza) {
        this.cabeza = cabeza;
    }

    public int getCola() {
        return cola;
    }

    public void setCola(int cola) {
        this.cola = cola;
    }

    public int getManzana() {
        return manzana;
    }

    public void setManzana(int manzana) {
        this.manzana = manzana;
    }

    public int getRunning() {
        return running;
    }

    public void setRunning(int running) {
        this.running = running;
    }

    public int getLost() {
        return lost;
    }

    public void setLost(int lost) {
        this.lost = lost;
    }

    public int getReady() {
        return ready;
    }

    public void setReady(int ready) {
        this.ready = ready;
    }

    public int getPosicionactual() {
        return posicionactual;
    }

    public void setPosicionactual(int posicionactual) {
        this.posicionactual = posicionactual;
    }

    public int getEstadoJuego() {
        return estadoJuego;
    }

    public void setEstadoJuego(int estadoJuego) {
        this.estadoJuego = estadoJuego;
    }

    public List<Coordenadas> getMuros() {
        return muros;
    }

    public void setMuros(List<Coordenadas> muros) {
        this.muros = muros;
    }

    public List<Coordenadas> getCulebra() {
        return culebra;
    }

    public void setCulebra(List<Coordenadas> culebra) {
        this.culebra = culebra;
    }

    public static final int ancho = 28;
    public static final int alto = 42;
    int x,y;
    int arriba=1,abajo=3,derecha=2,izquierda=4;
    int nada=0;
    int muro=1;
    int cabeza=2;
    int cola=3;
    int manzana=4;
    int running=1;
    int lost=2;
    int ready=3;
    int puntaje=0;
    int posicionactual = derecha;
    int estadoJuego = running;
    boolean incrementar = false;



    private List<Coordenadas> muros = new ArrayList<>();
    private List<Coordenadas> culebra = new ArrayList<>();//
    private List<Coordenadas> pera = new ArrayList<>();
    public GameMaster() {
    }


    public void updateDirection(int nuevaDireccion){


       if(Math.abs(nuevaDireccion - posicionactual) % 2 == 1){
            posicionactual = nuevaDireccion;
        }

    }


    public void update(){
        //actualiza la culebrita

        switch (posicionactual) {
            case 1:
                updateSnake(0,-1);
                break;
            case 2:
                updateSnake(1,0);
                break;
            case 3:
                updateSnake(0,1);
                break;
            case 4:
                updateSnake(-1,0);
                break;
        }
        //revisa si se estrello con el muro
        for (Coordenadas w:muros) {
            if (culebra.get(0).equals(w)){
                estadoJuego = lost;
            }
        }
        for (Coordenadas m:pera) {
            if(culebra.get(0).equals(m)){
                incrementar = true;
                addpera();
                puntaje++;

            }
        }
        for (int i = 1; i <culebra.size() ; i++) {
            if (culebra.get(0).equals(culebra.get(i))){
                estadoJuego = lost;
                return;
            }
        }
        /*for (Coordenadas s:culebra) {
           if(culebra.get(0).equals(s) && !s.equals(culebra.get(0))){
               estadoJuego = lost;
           }
        }*/
    }

    public int [][] generarTablero(){
        int  [][] mapa = new int [ancho][alto] ;
        //dibuja toda la matriz

        for (int x = 0; x < ancho ; x++) {
            for (int y = 0; y < alto ; y++) {
                 if(x == 0 || x == ancho-1 || (y == 0 && x !=0 && x != ancho-1) || (y == alto-1 && x != 0 && x != ancho-1)){
                    mapa[x][y] = muro;
                    muros.add(new Coordenadas(x,y));
                 }else {
                    mapa[x][y] = nada;//nada
                }

            }
        }

        for (Coordenadas s:culebra) {
            mapa[s.getX()][s.getY()] = cola;
        }
        mapa[culebra.get(0).getX()][culebra.get(0).getY()] = cabeza;

        mapa[pera.get(0).getX()][pera.get(0).getY()] = manzana;

        return mapa;
    }

    private void updateSnake(int x, int y){
        int colax=0,colay=0;
        for (int i = culebra.size()-1; i > 0 ; i--) {
            if(incrementar){
                colax=culebra.get(i).getX();
                colay=culebra.get(i).getY();
            }
            culebra.get(i).setX(culebra.get(i-1).getX());
            culebra.get(i).setY(culebra.get(i-1).getY());
            if(incrementar){
                culebra.add(new Coordenadas(colax,colay));
                incrementar = false;
            }
        }

        culebra.get(0).setX(culebra.get(0).getX()+x);
        culebra.get(0).setY(culebra.get(0).getY()+y);



    }

    public void addculebra() {
        culebra.clear();
        culebra.add(new Coordenadas(8,8));//2 5/7
        culebra.add(new Coordenadas(7,8));//0=7/7
        culebra.add(new Coordenadas(6,8));//1 6/7

    }
    public void addpera(){

        pera.clear();
        x = (int) (Math.random()*26+1);
        y = (int) (Math.random()*40+1);
        boolean z = true;
        for (Coordenadas s:culebra) {
            if(s.getX() == x && s.getY() == y){
                z=false;
            }
        }
        if(z){
            pera.add(new Coordenadas (x,y));
        }else{
            addpera();
        }
        //pera.add();
    }



}
