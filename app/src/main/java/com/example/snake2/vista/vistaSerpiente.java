package com.example.snake2.vista;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.snake2.modelo.GameMaster;

public class vistaSerpiente extends View {
    private GameMaster gameMaster;
    private Paint mPaint = new Paint();
    private int vistaSerpiente [][];
    public vistaSerpiente(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public int[][] getVistaSerpiente() {
        return vistaSerpiente;
    }

    public void setVistaSerpiente (int [][] mapa){
        this.vistaSerpiente = mapa;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(vistaSerpiente != null){
            float tileSizeX = canvas.getWidth() /vistaSerpiente.length;
            float tileSizeY = canvas.getHeight() /vistaSerpiente[0].length;


            float CircleSize = Math.min(tileSizeX,tileSizeY)/2;
            for(int x=0; x<vistaSerpiente.length ; x++){
                for(int y=0; y<vistaSerpiente[x].length ; y++){
                    switch (vistaSerpiente[x][y]){
                        case 0:
                            mPaint.setColor(Color.TRANSPARENT);
                            break;
                        case 1:
                            mPaint.setColor(Color.BLUE);
                            break;
                        case 2:
                            mPaint.setColor(Color.RED);
                            break;
                        case 3:
                            mPaint.setColor(Color.BLUE);
                            break;
                        case 4:
                            mPaint.setColor(Color.RED);
                            break;
                    }

                    canvas.drawCircle(x * tileSizeX + tileSizeX/2 + CircleSize/2,
                            y *  tileSizeY + tileSizeY/2 + CircleSize/2,CircleSize,mPaint);
                }
            }
        }
    }

}
