package com.example.snake2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.example.snake2.modelo.GameMaster;
import com.example.snake2.vista.vistaSerpiente;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {
    private GameMaster gameMaster;
    private com.example.snake2.vista.vistaSerpiente vistaSerpiente;
    private final Handler handler = new Handler();
    private long tiempo = 300;
    private final long seg = 1000;
    int segundos=0,minutos=0,hora=0;
    private TextView puntaje;
    private TextView clock;
    float x1,y1;
    boolean state = true;
    int puntaje2 = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gameMaster = new GameMaster();
        gameMaster.addculebra();
        gameMaster.addpera();
        //gameMaster.generarTablero();
        puntaje =  (TextView) findViewById(R.id.puntaje);
        clock= (TextView)findViewById(R.id.clock);
        vistaSerpiente = (vistaSerpiente)findViewById(R.id.vistaserpiente);
        vistaSerpiente.setOnTouchListener(this);



        startUpdateHandler();
        reloj();

    }

    private void startUpdateHandler(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                gameMaster.update();
                if (gameMaster.getEstadoJuego() == gameMaster.getRunning()){
                    if (tiempo > 50){
                        if(puntaje2 < gameMaster.getPuntaje()){
                            tiempo-=10;
                            puntaje2++;
                        }
                    }
                    handler.postDelayed(this, tiempo);
                }
                if (gameMaster.getEstadoJuego() == gameMaster.getLost()){
                    perdida();
                    state=false;
                }

                vistaSerpiente.setVistaSerpiente(gameMaster.generarTablero());
                puntaje.setText("Puntaje:" +gameMaster.getPuntaje());
                vistaSerpiente.invalidate();
            }
        },tiempo);
    }

    private void reloj(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (gameMaster.getEstadoJuego() == gameMaster.getRunning()){
                    handler.postDelayed(this, seg);
                }
                    segundos++;
                    if(segundos== 60){
                        minutos++;
                        segundos=0;
                    }else if(minutos== 60) {
                        hora++;
                        minutos=0;
                    }
                    clock.setText(""+hora+":"+minutos+":"+segundos);


            }
        },seg);


    }



    private void perdida() {
        new AlertDialog.Builder(this)
                .setTitle("culebrita")
                .setMessage("puntaje: "+puntaje2+"\n tiempo: "+hora+":"+minutos+":"+segundos+"\n usted perdio ¿desea volver a jugar?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tiempo=300;
                        gameMaster.setPuntaje(0);
                        puntaje2=segundos=minutos=hora=0;
                        gameMaster.getCulebra().clear();
                        gameMaster.setRunning(1);
                        gameMaster.setEstadoJuego(1);
                        gameMaster.addculebra();
                        gameMaster.setPosicionactual(2);
                        startUpdateHandler();
                        reloj();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .show();
        Toast.makeText(this, "usted perdio",Toast.LENGTH_SHORT).show();
    }



    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                y1 = event.getY();
                break;
            case  MotionEvent.ACTION_UP:
                float x2 = event.getX();
                float y2 = event.getY();
                movimiento(x1, x2, y1, y2);
                break;
        }
        return true;
    }




    private void movimiento(float x1, float x2, float y1, float y2) {
        float difX = x2-x1;
        float difY = y2-y1;
        if(Math.abs(difX) > Math.abs(difY)){
            if(difX > 0){
                gameMaster.updateDirection(2);


            }else{
                gameMaster.updateDirection(4);


            }
        }else{
            if(difY > 0){
                gameMaster.updateDirection(3);

            }else{
                gameMaster.updateDirection(1);


            }
        }

    }
}